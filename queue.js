let collection = [];
// Write the queue functions below.

function enqueue(element) {
  collection[collection.length] = element;
  return collection;
}

function print() {
    return collection;
}


function dequeue() {
    for(let i = 0; i<collection.length; i++){
        collection[i] = collection[i+1];
    }
    collection.length--;
    return collection;
}

function front() {
    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
   if (collection.length > 0 ){
    isEmpty = false;
    return isEmpty;
   } else {
    isEmpty = true;
    return isEmpty;
   }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
